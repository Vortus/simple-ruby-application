# Partial includes
require './helpers/date_helpers'
require './extensions/contact_extension'
require './extensions/location_extension'
require './extensions/experience_extension'
require './extensions/skills_extension'
require './extensions/git_repo_extension'
require './extensions/education_extension'
require './curriculum_vitae'

# Main procedure
# puts "#{DateHelpers.timestamp} - Program started"
# puts "#{DateHelpers.timestamp} - Creating instances"

# Locations
# puts "#{DateHelpers.timestamp} - Creating location instances"
kaunasHome = LocationExtension.new(LocationExtension.regions[3], 'Lithuania',
    'Kaunas', 'Vytenio g. 16')
kaunasH1P = LocationExtension.new(LocationExtension.regions[3], 'Lithuania',
  'Kaunas', 'Savanorių pr. 109')
kaunasKTU = LocationExtension.new(LocationExtension.regions[3], 'Lithuania',
    'Kaunas', 'K. Donelaičio g. 73')
kaunasKTU = LocationExtension.new(LocationExtension.regions[3], 'Lithuania',
    'Kretinga', 'J. Pabrėžos g. 4')

# Contact information
# puts "#{DateHelpers.timestamp} - Creating contact instances"
ugniusContact = ContactExtension.new('Ugnius', 'Skučas', '19',
  '+37060468143', 'ugnius.skucas@gmail.com', kaunasHome)

# Workplaces
# puts "#{DateHelpers.timestamp} - Creating workplace instances"
experience_array = []
experience_array << ExperienceExtension.new('Host1Plus', kaunasH1P,
  'Support Staff Engineer', 'Aug 2016', 'Present')

# Skills
# puts "#{DateHelpers.timestamp} - Creating skill instances"
skills = SkillsExtension.new(['Linux', 'Ubuntu', 'CentOS', 'Debian',
  'Kali Linux', 'Windows', 'Android', 'Programming', 'Ruby on Rails', 'Ruby',
  'C++', 'C#', 'Java', 'Game Development', 'PHP', 'CSS', 'HTML', 'JavaScript',
  'WordPress', 'WHM', 'cPanel', 'DirectAdmin',
  'jQuery', 'MySQL', 'PostgreSQL', 'SQL', 'Microsoft Word', 'Microsoft Excel',
  'Microsoft PowerPoint'])
skills.add_skill('JSON', 17)

skills.add_skill('bug')
skills.remove_skill()

# Git repos
# puts "#{DateHelpers.timestamp} - Creating Git repo instances"
git_repositories = []
git_repositories << GitRepoExtension.new('Travel App with React Native (Unfinished)',
  'https://bitbucket.org/Vortus/travelapp')
git_repositories << GitRepoExtension.new('FastParrot/IPTils service pinger with ROR',
  'https://bitbucket.org/Vortus/fastparrot-pinger')
git_repositories << GitRepoExtension.new(
  'Code sharing platform with ROR http://bitslace.herokuapp.com/',
  'https://bitbucket.org/Vortus/official-bitslace/commits/all')
git_repositories << GitRepoExtension.new('Simple facebook login with ROR',
    'https://bitbucket.org/Vortus/facebook-login')
git_repositories << GitRepoExtension.new('Android picture game with Java',
    'https://bitbucket.org/Vortus/picture-game-android')
git_repositories << GitRepoExtension.new('Large C++ source code exercise database',
    'https://bitbucket.org/Vortus/c-source-code')
git_repositories << GitRepoExtension.new('Unity3D number game written with C#',
    'https://bitbucket.org/Vortus/unity3d-game-numberer-android')
git_repositories << GitRepoExtension.new(
    'Captures three most dominant collors from image, written on Java',
    'https://bitbucket.org/Vortus/colorpicker-android')
git_repositories << GitRepoExtension.new('Lithuanian Olympic exercises C++',
    'https://bitbucket.org/Vortus/lmio-u-duotys')
git_repositories << GitRepoExtension.new('Kaunas Technology University olympic exercises C++',
    'https://bitbucket.org/Vortus/ktu-programavimo-u-duotys')
git_repositories << GitRepoExtension.new('Basic exercises for C++',
    'https://bitbucket.org/Vortus/iuolaiki-kas-vilgsnis-programavimo-pagrindus-11-12-kl')
git_repositories << GitRepoExtension.new('BitBucket Account',
    'https://bitbucket.org/vortus')

# Education
# puts "#{DateHelpers.timestamp} - Creating Education instances"
education_array = []
education_array << EducationExtension.new('Kaunas University of Technology', kaunasKTU,
  'Computer Programming', '2016', '2020')
education_array << EducationExtension.new('VšĮ Pranciškonų Gimnazija', kaunasKTU,
    'High School', '2004', '2016')

# Summarize
cv = CurriculumVitae.new(ugniusContact, kaunasHome, experience_array,
  skills, git_repositories, education_array)

cv.stdout
