module DateHelpers
  # Returns current formated system time
  def self.timestamp
    "[#{Time.now.strftime("%H:%M:%S")}]"
  end
end
