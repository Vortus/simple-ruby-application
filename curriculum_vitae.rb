class CurriculumVitae
  # Timestamp helper
  require './helpers/date_helpers'

  # Curriculum Vitae extensions
  require './extensions/contact_extension'
  require './extensions/location_extension'
  require './extensions/experience_extension'
  require './extensions/skills_extension'
  require './extensions/git_repo_extension'
  require './extensions/education_extension'

  # Class Attributes
  attr_accessor :contact
  attr_accessor :location
  attr_accessor :experience_array
  attr_accessor :skills
  attr_accessor :git_repositories
  attr_accessor :education_array

  # Class constructor
  def initialize(contact=nil, location=nil, experience_array=[],
      skills=nil, git_repositories=[], education_array=[])
    @contact = contact
    @location = location
    @experience_array = experience_array
    @skills = skills
    @git_repositories = git_repositories
    @education_array = education_array
  end

  # Standart output
  def stdout
    @contact.stdout
    @location.stdout
    @experience_array.each { |x| x.stdout }
    @skills.stdout
    @git_repositories.each { |x| x.stdout }
    @education_array.each { |x| x.stdout }
  end
end
