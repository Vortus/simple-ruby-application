require './extensions/extension'

class SkillsExtension < Extension
  # Skills attributes
  attr_accessor :skills

  # Class constructor
  def initialize(skills=[])
    @skills = skills
  end

  # Insert skill to array
  def add_skill(skill, position=-1)
    @skills.insert(position, skill) if skill
  end

  # Delete skill from array
  def remove_skill(position=-1)
    @skills.delete_at(position) if position
  end

  # Returns class object as string
  def to_s
    @skills.join(', ')
  end
end
