require './extensions/extension'
require './extensions/location_extension'

class ExperienceExtension < Extension
  # Experience attributes
  attr_accessor :company
  attr_accessor :location
  attr_accessor :position
  attr_accessor :started
  attr_accessor :finished

  # Class constructor
  def initialize(company='none', location=nil,
      position='none', started='none', finished='none')
    @company = company
    @location = location
    @position = position
    @started = started
    @finished = finished
  end

  # Return class object as string
  def to_s
    "#{@company}" +
    "#{", #{location}, " unless location.nil?}" + # Append location data
    "#{@position} #{@started} #{@finished}"
  end
end
