require './extensions/extension'
require './extensions/location_extension'

class ContactExtension < Extension
  # Contact attributes
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :age
  attr_accessor :phone_number
  attr_accessor :email
  attr_accessor :location

  # Class constructor
  def initialize(first_name='none', last_name='none', age='none',
      phone_number='none', email='none', location=nil)
    @first_name = first_name
    @last_name = last_name
    @age = age
    @phone_number = phone_number
    @email = email
    @location = location
  end

  # Return class object as string
  def to_s
    "#{@first_name}, #{@last_name}, #{@age}, #{@phone_number}, #{@email}" +
    "#{", #{location}" unless location.nil?}" # Append location data
  end
end
