require './extensions/extension'
require './extensions/location_extension'

class EducationExtension < Extension
  # Educaion attributes
  attr_accessor :school
  attr_accessor :location
  attr_accessor :study
  attr_accessor :started
  attr_accessor :finished

  # Class constructor
  def initialize(school='none', location=nil,
      study='none', started='none', finished='none')
    @school = school
    @location = location
    @study = study
    @started = started
    @finished = finished
  end

  # Return class object as string
  def to_s
    "#{@school}" +
    "#{", #{location}, " unless location.nil?}" + # Append location data
    "#{@study} #{@started} #{@finished}"
  end
end
