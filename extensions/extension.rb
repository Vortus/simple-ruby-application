class Extension
  require './helpers/date_helpers'

  # Display extension to standart output
  def stdout
    puts "#{DateHelpers.timestamp} #{self.to_s}"
  end
end
