require './extensions/extension'

class LocationExtension < Extension
  # location attributes
  attr_accessor :region
  attr_accessor :country
  attr_accessor :city
  attr_accessor :address

  # Regions as class variable
  @@regions = ['Africa', 'Asia', 'Central America', 'Eastern Europe',
    'European Union', 'Middle East', 'North America', 'Oceania',
    'South America', 'The Caribbean']

  # Retrieve class variable region list
  def self.regions
    @@regions
  end

  # Class constructor
  def initialize(region=@@regions[0],
      country='none', city='none', address='none')
    @region = region
    @country = country
    @city = city
    @address = address
  end

  # Returns class object as string
  def to_s
    "#{@region}, #{@country}, #{@city}, #{@address}"
  end
end
