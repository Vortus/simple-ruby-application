require './extensions/extension'

class GitRepoExtension < Extension
  # Git attributes
  attr_accessor :description
  attr_accessor :repository

  # Class constructor
  def initialize(description='none', repository='none')
    @description = description
    @repository = repository
  end

  # Returns class object as string
  def to_s
    "#{@description} - #{@repository}"
  end
end
